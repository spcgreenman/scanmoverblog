<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class scanmovers extends Controller
{
    /**
     * @Route("/scanmovers")
     */
    public function showAction()
    {
        $string = '';
        for ($i=1; $i < 101; $i++)
        {
            if ($i % 3 == 0)
            {
                if ($i % 5 == 0)
                {
                    $string .= "SCANMOVERS" . "<br/>";
                }
                else
                {
                    $string .= "SCAN" . "<br/>";
                }
                
            }
            elseif ($i % 5 == 0)
            {
                $string .= "MOVERS" . "<br/>";
            }
            else
            {
                $string .= (string)$i . "<br/>";
            }
        }
        return new Response($string);
    }
}