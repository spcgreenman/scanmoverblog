<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return new Response(
            '<html><body>
                <a href=/scanmovers>scanmovers<a> <br/>
                <a href=/getblogposts>get blog posts<a> <br/>
                <a href=/createblogpost>create blog post<a> <br/>
                <a href=/viewblogposts>view blog posts<a> <br/>
            </body></html>
            ');
    }

}

?>