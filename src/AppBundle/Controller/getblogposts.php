<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class getblogposts extends Controller
{
    /**
     * @Route("/getblogposts")
     */
    public function showAction()
    {
        $path = $this->get('kernel')->getRootDir() . '/Resources/data/blogposts.json';
        
        $str = file_get_contents($path);

        return new jsonResponse($str);
    }
}