<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class deleteblogpost extends Controller
{
    /**
     * @Route("/deleteblogpost")
     */
    public function showAction(\Swift_Mailer $mailer)
    {
        $path = $this->get('kernel')->getRootDir() . '/Resources/data/blogposts.json';
        
        $str = file_get_contents($path);

        $json = json_decode($str, TRUE);
        
        //explanation of how email works https://symfony.com/doc/current/email.html
        $message = (new \Swift_Message('Notification email'))
        ->setFrom('sender@example.com')
        ->setTo('recipient@example.com')
        ->setBody('Post with text: (' . $json['data'][$_GET["postID"]]['text'] . ') was deleted');

        $mailer->send($message);

        unset($json['data'][$_GET["postID"]]);

        file_put_contents($path, json_encode($json));

        return $this->redirect('http://localhost:8000/viewblogposts');
    }

}