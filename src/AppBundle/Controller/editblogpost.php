<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class editblogpost extends Controller
{
    /**
     * @Route("/editblogpost")
     */
    public function showAction()
    {
        $path = $this->get('kernel')->getRootDir() . '/Resources/data/blogposts.json';
        
        $str = file_get_contents($path);

        $json = json_decode($str, TRUE);

        $formstring = 
        '<html><body>
                <form id="postform" action="/addblogpost" method="post">
                    <input type="hidden" value=' . $_GET["postID"] .' name="postID"/>
                    <select name="published" form="postform">
                        <option value="published">published</option>
                        <option value="non-published">non-published</option>
                    </select>
                    post:<br>
                    <textarea name="posttext" form="postform" cols="40" rows="5"></textarea>
                    <input type="submit" value="Submit">
                </form>
            </body></html>';

        return new Response($formstring);
    }

}