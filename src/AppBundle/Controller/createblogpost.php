<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class createblogpost extends Controller
{
    /**
     * @Route("/createblogpost")
     */
    public function showAction()
    {

        return new Response(
            '<html><body>
                <form id="postform" action="/addblogpost" method="post">
                    <input type="hidden" value=-1 name="postID"/>
                    <select name="published" form="postform">
                        <option value="published">published</option>
                        <option value="non-published">non-published</option>
                    </select>
                    post:<br>
                    <textarea name="posttext" form="postform" cols="40" rows="5"></textarea>
                    <input type="submit" value="Submit">
                </form>
            </body></html>
            ');
    }

}