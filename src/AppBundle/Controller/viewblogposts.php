<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class viewblogposts extends Controller
{
    /**
     * @Route("/viewblogposts")
     */
    public function showAction()
    {
        $path = $this->get('kernel')->getRootDir() . '/Resources/data/blogposts.json';
        
        $str = file_get_contents($path);

        $json = json_decode($str, TRUE);

        $posts = '<html><body><a href=/>home<a><br/><br/>
        <form id="sortform" action="" method="get">
            <select name="sort" form="sortform">
                <option value="new">newest first</option>
                <option value="old">oldest first</option>
            </select>
            <input type="submit" name="button" value="sort"/>
        </form>';

        $reverse = FALSE;

        if (isset($_GET["sort"]))
        {
            if ($_GET["sort"] == "new")
            {
                $reverse = TRUE;
            }
        }

        $data = $json['data'];

        if ($reverse)
        {
            $data = array_reverse($data);
        }

        foreach ($data as $key => $value) 
        {
            $published = 'not published';
            if ($value['published'] == 'published')
            {
                $published = 'published';
            }
            $posts .= 'blogpost:
            <form action="/deleteblogpost" method="get">
                <input type="hidden" value=' . $key .' name="postID"/>
                <input type="submit" value="delete"> 
            </form>
            <form action="/editblogpost" method="get">
                <input type="hidden" value="' . $key .'" name="postID"/>
                <input type="submit" value="edit"> 
            </form>
            text: ' . $value['text'] . '<br/>' .
                       $published . '<br/>' .
                       'date: ' . $value['date'] . '<br/><br/><br/>';
        }

        return new Response($posts);
    }
}