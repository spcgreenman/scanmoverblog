<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


function sanitize($input) { return htmlspecialchars(trim($input)); }

class addblogpost extends Controller
{
    /**
     * @Route("/addblogpost")
     */
    public function showAction(\Swift_Mailer $mailer)
    {
        $path = $this->get('kernel')->getRootDir() . '/Resources/data/blogposts.json';
        
        $str = file_get_contents($path);

        $datetime = date('Y-m-d H:i:s');

        $json = json_decode($str, TRUE);

        $published = 0;
        if (sanitize($_POST["published"]) == "published")
        {
            $published = 1;
        }

        $newpost = array("text" => sanitize($_POST["posttext"]), "published" => $published, "date" => $datetime);

        $emailbody = '';
        $postID = sanitize($_POST["postID"]);
        if ($postID == -1)
        {
            $emailbody = 'blogpost added: ' . sanitize($_POST["posttext"]);
            array_push($json['data'], $newpost);
        }
        else
        {
            $emailbody = 'blogpost editied: 
                         old text: (' . $json['data'][$postID]['text'] . 
                         ') new text: (' . sanitize($_POST["posttext"]) . ')';
            $json['data'][$postID]['text'] = $newpost['text'];
            $json['data'][$postID]['published'] = $newpost['published'];
        }

        file_put_contents($path, json_encode($json));
        //explanation of how email works https://symfony.com/doc/current/email.html
        $message = (new \Swift_Message('Notification email'))
        ->setFrom('sender@example.com')
        ->setTo('recipient@example.com')
        ->setBody($emailbody);

        $mailer->send($message);
        return $this->redirect('http://localhost:8000/viewblogposts');
    }

}